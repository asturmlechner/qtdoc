# Copyright (C) 2022 The Qt Company Ltd.
# SPDX-License-Identifier: BSD-3-Clause

target_link_libraries(helloworld PRIVATE
    businesslogic
    Qt6::Widgets)
